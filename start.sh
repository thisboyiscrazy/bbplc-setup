#!/bin/bash

sudo cp files/jjvanderstelts.config /var/lib/connman/

NEWHOST=`cat files/hostname`

echo $NEWHOST > /etc/hostname

sed -e s/beaglebone/$NEWHOST/g /etc/hosts > files/hosts.tmp

mv files/hosts.tmp /etc/hosts

systemctl enable systemd-networkd-wait-online.service 

cp post_install.service /etc/systemd/system/

systemctl enable post_install

sudo /opt/scripts/tools/grow_partition.sh

systemctl mask getty@tty1.service

reboot
