#!/bin/bash

write() {
	echo -n -e "$1" | dd of=/sys/bus/i2c/devices/2-0054/eeprom bs=1 seek=$2
}

dd if=/dev/zero of=/sys/bus/i2c/devices/2-0054/eeprom bs=1 count=243

#HEADER
write '\xAA\x55\x33\xEE' 0
write 'A1' 4
write 'BBIM' 6
write '00A0' 38
write 'NC-TB' 42
write 'BBIM' 58
write '21' 74

#SERIAL
WY=$(date +"%W%y")

write "$(date +"%W%y")" 76
write 'C' 80


write '1' 84

#ARDUINO
write '\x80\x06' 124
write '\x80\x06' 126
write '\x80\x07' 122

#CAN
write '\x80\x02' 112
write '\x80\x02' 110

#INPUTS
write '\x80\x07' 220
write '\x80\x07' 216
write '\x80\x07' 214
write '\x80\x07' 210
write '\x80\x07' 212
write '\x80\x07' 218
write '\x80\x07' 69
write '\x80\x07' 114

#LED
write '\x80\x07' 170
write '\x80\x07' 176
write '\x80\x07' 172
write '\x80\x07' 174

#OUT
write '\x80\x07' 146
write '\x80\x07' 144
write '\x80\x07' 120

#VOLTAGE
write '\x00\x00' 236
write '\x00\x00' 238
write '\x00\x00' 240
write '\x05\xDC' 242

dd if=/dev/zero of=/sys/bus/i2c/devices/2-0054/eeprom bs=1 seek=244
