#!/bin/bash


while true
do
	cat /sys/class/gpio/gpio110/value | tr -d '\n'
	cat /sys/class/gpio/gpio111/value | tr -d '\n'
	cat /sys/class/gpio/gpio112/value | tr -d '\n'
	cat /sys/class/gpio/gpio113/value | tr -d '\n'
	cat /sys/class/gpio/gpio7/value | tr -d '\n'
	cat /sys/class/gpio/gpio115/value | tr -d '\n'
	cat /sys/class/gpio/gpio20/value | tr -d '\n'
	cat /sys/class/gpio/gpio117/value | tr -d '\n'

	echo

	sleep 1
done
