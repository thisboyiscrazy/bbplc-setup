#!/bin/bash

config-pin P8-09 low
config-pin P8-10 low
config-pin P8-11 low
config-pin P8-12 low

config-pin P9-31 in
config-pin P9-29 in
config-pin P9-30 in
config-pin P9-28 in
config-pin P9-42 in
config-pin P9-27 in
config-pin P9-41 in
config-pin P9-25 in

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

