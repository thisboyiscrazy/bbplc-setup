#!/bin/bash

LOGDIR="logs"
LOGEXT="log"

if [ ! -e $LOGDIR ]
then
    mkdir $LOGDIR
fi

WY=$(date +%W%y)
LOG=${LOGDIR}/${WY}.${LOGEXT}

if [ -e ${LOG} ]
then
    COUNT=`cat ${LOG}`
else
    COUNT=0
fi

((COUNT++))

echo $COUNT > ${LOG}

echo $COUNT