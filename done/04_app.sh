#!/bin/bash

. ~/.bashrc

nvm install node
nvm use node

npm install -g yarn

cp -r files/ssh ~/.ssh

cd ~

git clone git@bitbucket.org:thisboyiscrazy/bbplc.git
