#!/bin/bash

sudo apt-get update
sudo apt-get -y dist-upgrade
sudo apt-get -y autoremove

sudo apt-get -y install bb-cape-overlays --reinstall

cat << EOF >> /boot/uEnv.txt

enable_uboot_overlays=1
disable_uboot_overlay_emmc=1
disable_uboot_overlay_audio=1
enable_uboot_cape_universal=1
EOF
