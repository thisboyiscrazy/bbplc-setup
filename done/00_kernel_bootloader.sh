#!/bin/bash 

dd if=/dev/zero of=/dev/mmcblk1 count=1 seek=1 bs=128k
dd if=/dev/zero of=/dev/mmcblk1 count=2 seek=1 bs=384k

rm /uEnv.txt

cd /opt/scripts/tools/developers/

echo y | ./update_bootloader.sh

cd /opt/scripts/tools/

sudo ./update_kernel.sh
