#!/bin/bash

for s in 'apache2.service cloud9.service node-red.service bonescript-autorun.service bonescript.service roboticscape.service'
do
	sudo systemctl disable $s
done

for s in 'bonescript.socket cloud9.socket node-red.socket'
do
	sudo systemctl disable $s
done
