#!/bin/bash

connmanctl tether wifi off
connmanctl enable wifi
connmanctl scan wifi
NETWORK=`connmanctl services | grep \* | grep -o wifi_[a-z0-9_]*`
connmanctl connect $NETWORK
