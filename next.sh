#!/bin/bash -e

cd /root/bbplc-setup/

if [ -f inprocess.lock ]
then
	echo "!!! Process failed"
	cat inprocess.lock
	exit 1
fi

FILE=`ls todo/ | head -n 1 -`

if [[ -z $FILE ]]; then
	wall "Installation Done"
	systemctl mask getty@tty1.service
	exit 0
fi

wall "Processing $FILE"

echo $FILE > inprocess.lock

bash todo/$FILE 2>&1 | tee -a done/$FILE.out
mv todo/$FILE done/

rm inprocess.lock

reboot
